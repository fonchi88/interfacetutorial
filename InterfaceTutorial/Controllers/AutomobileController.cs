﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.Controllers
{
    public class AutomobileController
    {
        IAutomobile m_Automobile;

        public AutomobileController(IAutomobile automobile) {
            this.m_Automobile = automobile;
        }

        public void Ignition() {
            m_Automobile.Ignition();
        }

        public void Stop() {
            m_Automobile.Stop();
        }
    }
}
