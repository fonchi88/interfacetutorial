﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTutorial.Interfaces
{
    public interface IAutomobile
    {
        void Ignition();
        void Stop();
    }
}
