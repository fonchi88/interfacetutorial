﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTutorial.Interfaces
{
    interface IAccount
    {
        decimal Balance { get; set; }
        decimal CalcInterest();
    }
}
