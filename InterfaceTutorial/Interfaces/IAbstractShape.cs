﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTutorial.Interfaces
{
    public abstract class IAbstractShape
    {
        public abstract string GetShape();
    }
}
