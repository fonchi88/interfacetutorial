﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTutorial.Interfaces
{
    public interface IOrder
    {
        void AddToCart(string prod);
    }
}
