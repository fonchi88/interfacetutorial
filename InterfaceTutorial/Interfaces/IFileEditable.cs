﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTutorial.Interfaces
{
    interface IFileEditable
    {
        void EditFile(string text);
    }
}
