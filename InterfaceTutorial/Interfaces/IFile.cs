﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTutorial.Interfaces
{
    interface IFile
    {
        string Name { get; set; }
        string Format { get; set; }
        void ReadFile();
        void WriteFile(string text);
    }
}
