﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    public class Accounts
    {
    }

    public class RegularSavingsAccount : IAccount {
        public decimal Balance { get; set; } = 0;
        public decimal CalcInterest() {
            decimal Interest = (Balance * 4) / 100;
            Console.WriteLine(Interest); 
            if (Balance < 1000) Interest -= (Balance * 2) / 100;
            Console.WriteLine(Interest); 
            if (Balance < 50000) Interest += (Balance * 4) / 100;
            Console.WriteLine(Interest);

            return Interest;
        }
    }
}
