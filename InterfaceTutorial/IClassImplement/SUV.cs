﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    public class SUV:IAutomobile
    {
        public void Ignition() {
            Console.WriteLine("Start SUV...");
        }

        public void Stop() {
            Console.WriteLine("Stop SUV...");
        }
    }
}
