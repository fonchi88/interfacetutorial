﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    class FileWord:IFile,IFileEditable
    {
        public string Name { get; set; }
        public string Format { get; set; } = "docx";

        private string _Text;
        public void ReadFile()
        {
            Console.WriteLine("Reding your file");
        }

        public void WriteFile(string text)
        {
            _Text = text;
            Console.WriteLine("This is your text: " + _Text);
        }

        public void EditFile(string text) {

            _Text = String.Concat(_Text,text);
            Console.WriteLine("This is your new text: " + _Text);
        }
    }
}
