﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    public class Figure
    {
        public void HasVertex(List<IVertex> figure) {
            foreach (var fig in figure) {
                fig.GetVertexAmount();
            }
        }
    }

    public class Circle : IAbstractShape {
        public override string GetShape() {
            return "Circle";
        } 
    }

    public class Triangle : IAbstractShape, IVertex
    {
        public override string GetShape()
        {
            return "Triangule";
        }

        public int GetVertexAmount() {
            return 3;
        }
    }
}
