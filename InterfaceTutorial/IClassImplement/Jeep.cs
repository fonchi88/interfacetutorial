﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    public class Jeep:IAutomobile
    {
        public void Ignition() {
            Console.WriteLine("Starting Jeep...");
        }

        public void Stop() {
            Console.WriteLine("Stopping Jeep");
        }
    }
}
