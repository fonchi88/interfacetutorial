﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    public class FilePdf : IFile
    {
        public string Name { get; set; }
        public string Format { get; set; } = "PDF";
        public void ReadFile()
        {
            Console.WriteLine("Reding your file");
        }

        public void WriteFile(string text) {
            Console.WriteLine("This is your text: "+text);
        }

    }
}
