﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceTutorial.Interfaces;

namespace InterfaceTutorial.IClassImplement
{
    public class OnlineOrder:IOrder,IOnlineOrder
    {
        public void AddToCart(string product) {
            Console.WriteLine("Add Item "+product+"...");
        }

        public void CCProcess() {
            Console.WriteLine("Credit Card charge");
        }
    }
}
