﻿using System;
using InterfaceTutorial.IClassImplement;
using InterfaceTutorial.Interfaces;
using InterfaceTutorial.Controllers;

namespace InterfaceTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            FilePdf file = new FilePdf();


            file.ReadFile();
            file.WriteFile("Now");
            Console.WriteLine(file.Format);

            FileWord docx = new FileWord();
            docx.ReadFile();
            docx.WriteFile("Now");
            docx.EditFile(" Test");
            docx.WriteFile("Change");
            docx.EditFile(" Text");

            RegularSavingsAccount regularAccount = new RegularSavingsAccount();
            regularAccount.Balance = 12500;
            Console.WriteLine("---Regular Savings Account Interest---");
            Console.WriteLine(regularAccount.CalcInterest());

            IAbstractShape circle = new Circle();

            Console.WriteLine(circle.GetShape());

            IVertex triangle = new Triangle();
            Console.WriteLine(triangle.GetVertexAmount());

            OnlineOrder order1 = new OnlineOrder();

            order1.AddToCart("iPod");
            order1.CCProcess();

            IAutomobile automobile = new Jeep();
            AutomobileController automobileController = new AutomobileController(automobile);

            automobileController.Ignition();
            automobileController.Stop();

        }
    }
}
